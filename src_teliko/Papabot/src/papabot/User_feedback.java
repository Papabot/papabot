package papabot;

public class User_feedback extends Feedback{
	static private int count = 0;
	private String report_fd;
	private int r_num;
	
	public User_feedback(RegisteredUser ru, int u_id, int fdback_id, String date, int f_num, String report_fd){
		super(ru, u_id, fdback_id, date);
		this.r_num = ++count;
		this.report_fd = report_fd; 
	}
	
	public int count_reports(){
		if(r_num>30){
			System.out.println("Delete user with id: " + u_id);
			return r_num;
		} else{
			System.out.println("User " + u_id + " has less than 30 reports.");
			return r_num;
		}
	}
}
