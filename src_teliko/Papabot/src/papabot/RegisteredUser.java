package papabot;

import java.util.ArrayList;
import java.util.*;
import java.io.IOException;
import java.util.Arrays;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.io.StringReader;

public class RegisteredUser extends User {
	static private int id = 0;

	List<RegisteredUser> friends = new ArrayList<RegisteredUser>();

	private String username;
	private int usr_id;
	private enum status{
		Online,
		Offline,
		Busy
	}
	private String email;
	private String password;
	private String host;
	private int port;
	
	RegisteredUser(String name, String lastname, int usr_id, String username, String email, String password,String host, int port) {
		super(name, lastname);
		this.usr_id = ++id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.host = host;
		this.port = port;
	}
	public void add_picture_prof() {

	}

	public void search_friend(String username) {
		for(RegisteredUser i:friends) {
			if(i.username == username) {
				System.out.println("Friend found");
				break;
			}
		}
	}
	ChatWithUser user = new ChatWithUser();
	void add_contact(RegisteredUser contact) {
		friends.add(contact);
	}

	void change_status(String state) {
		if (state == "Offline") {
			s1 = status.Offline;
		} else if(state == "Busy") {
			s1 = status.Busy;
		} else {
			s1 = status.Online;
		}
	}
	void delete_contact(String username) {
		for(RegisteredUser i:friends) {
			if(i.username == username) {
				friends.remove(i);
				System.out.println("Friend deleted");
				break;
			}
		}
	}
}
