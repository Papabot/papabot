package papabot;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Bot extends JFrame  {
	
	private JTextArea Chatarea = new JTextArea();
	private JTextField chatbox = new JTextField();
	private JButton Send = new JButton("Send");
	
	public Bot() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLayout(null);
		frame.setSize(500, 600);
		frame.setTitle("ChatBot");
		frame.add(Chatarea);
		frame.add(chatbox);
		frame.add(Send);
		//for ttextarea
		Chatarea.setSize(500, 300);
		Chatarea.setLocation(2, 2);
		
		//for textfield
		chatbox.setSize(490, 20);
		chatbox.setLocation(2, 450);
		
		//
		Send.setSize(90, 20);
		Send.setLocation(2, 50);
		
		chatbox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String gtext = chatbox.getText();
				Chatarea.append("You-->" + gtext + "\n");
				chatbox.setText("");
				
				if(gtext.contains("Hi")) {
					bot("Geia kai se esena");
				}
				
				else
					bot("den katalava thn erwthsh sas");
				
			}
		});
		
		}
	private void bot(String string) {
		Chatarea.append("Bot-->" +string+ "\n") ;
	}
	
	


}
