package papabot;

public class Feedback {
	static private int f_id=0;
	private RegisteredUser ru;
	private int fdback_id;
	private String date;
	protected int u_id; 
	
	public Feedback(RegisteredUser ru, int fdback_id, int u_id, String date){
		this.ru = ru;
		this.u_id = u_id;
		this.fdback_id = ++f_id;
		this.date = date;
	}

	public void show_info(){
		System.out.println("The feedback made by user: " + u_id);
		System.out.println("Date: " + date);
	}
}
