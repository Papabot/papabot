package papabot;

public class Chatbot_feedback extends Feedback {
	static private int rate = 0;
	private String question;
	private boolean fdback_rate;
	
	public Chatbot_feedback(RegisteredUser ru, int u_id, int fdback_id, String date, String question, boolean fdback_rate){
		super(ru, u_id, fdback_id, date);
		this.question = question;
		this.fdback_rate = fdback_rate;
	}

	
	public int count_feedback(){
		if(fdback_rate == true){
			rate += 1;
		} else{
			rate -= 1;
		}
		return rate;
	}
}